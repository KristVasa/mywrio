# MyWRIO
## C++ API for (National Instruments) myRIO 1950

C++ framework for a myRIO 1950. The goal of this framework is to be able to code this tool with C++ without using LabVIEW. For the developement of this API we used the official C API provided by NI and added a new level of abstraction to handle the registers and all low level programing.

[**Documentation**](https://gitlab.com/KristVasa/mywrio_doc/)

*Documented with Doxygen.*
